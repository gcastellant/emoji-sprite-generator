module.exports.toDecimal = (str, radix = 16) => {
  let count = 0;
  let digit = 0;
  let accumulation = 0;
  let haveAccumulation = false;

  const result = [];

  for (let i = 0; i < str.length; ++i) {
    const c = str.charCodeAt(i);

    switch (c) { // eslint-disable-line
      // 0..9
      case 0x30: case 0x31: case 0x32: case 0x33: case 0x34: case 0x35:
      case 0x36: case 0x37: case 0x38: case 0x39:
        digit = c - 0x30;
        if (digit >= radix) {
          return;
        }
        accumulation = (accumulation * radix) + digit;
        haveAccumulation = true;
        break;

      // a..f
      case 0x61: case 0x62: case 0x63: case 0x64: case 0x65: case 0x66:
        digit = c - 0x61 + 10;
        if (digit >= radix) {
          return;
        }
        accumulation = (accumulation * radix) + digit;
        haveAccumulation = true;
        break;

      // A..F
      case 0x41: case 0x42: case 0x43: case 0x44: case 0x45: case 0x46:
        digit = c - 0x41 + 10;
        if (digit >= radix) {
          return;
        }
        accumulation = (accumulation * radix) + digit;
        haveAccumulation = true;
        break;

      // comma, whitespace
      case 0x2C:
      case 0x09: case 0x0A: case 0x0B: case 0x0C: case 0x0D: case 0x20: case 0x85:
      case 0xA0: case 0x1680:
      case 0x2000: case 0x2001: case 0x2002: case 0x2003: case 0x2004:
      case 0x2005: case 0x2006: case 0x2007: case 0x2008:
      case 0x200A: case 0x200B:
      case 0x2029: case 0x2028: case 0x202F: case 0x3000:
        if (haveAccumulation) {
          result[count++] = accumulation;
          accumulation = 0;
          haveAccumulation = false;
        }
        break;
    }
  }

  if (haveAccumulation) {
    result[count++] = accumulation;
    accumulation = 0;
  }

  return result;
};