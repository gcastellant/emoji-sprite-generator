const fs = require('fs');
const path = require('path');
const mkdirp = require('mkdirp');
const base64 = require('base64js');
const sharp = require('sharp');
const rm = require('rm-r/sync');
const glob = require('glob');
const convert = require('./utils/convert');

const images = path.resolve(__dirname, './sprite');
const sass = path.resolve(__dirname, './sass');

const validRow = 17; // number of cols
const content = fs.readFileSync('./crawl.html', 'utf8').replace(/\n/gmi, '');
const separator = ',';
const skinTones = ['none', '1f3fb', '1f3fc', '1f3fd', '1f3fe', '1f3ff'];

// for good quality prefer even number
const emojiSizes = [20, 14];

// cols declaration
const declaration = {
  2: {
    key: 'code',
    rule: /name="([^"]+)"/gmi,
    index: 1,
    callback(code) {
      return {
        decimal: code.split('_').map(c => convert.toDecimal(c)[0]).join('_')
      };
    }
  },

  3: {
    key: 'ios',
    rule: /<img [^\>]+ src="([^"]+)">/gmi,
    index: 1,
    emoji: true
  },
  4: {
    key: 'android',
    rule: /<img [^\>]+ src="([^"]+)">/gmi,
    index: 1,
    emoji: true
  }
};

const platforms = Object.keys(declaration).reduce((acc, k) => {
  const { key, emoji } = declaration[k];

  if (emoji === true) {
    acc.push(key);
  }

  return acc;
}, []);

const cleanDirectory = (cb) => {
  rm(images);
  rm(sass);

  platforms.forEach(platform => {
    emojiSizes.forEach(size => {
      mkdirp.sync(`${images}/${platform}_${size}`);
    });
    mkdirp.sync(`${images}/tmp/${platform}`);
  });

  mkdirp.sync(sass);

  cb();
};

const buildSassFile = () => {
  const col = 1;

  emojiSizes.forEach(size => {
    platforms.forEach(platform => {
      glob(`${images}/${platform}_${size}/*.png`, (err, files) => {
        if (err || files.length === 0) return;

        const i = 0;
        const x = 1;
        const cleaned = files.map(file => file.split('/').pop().split('.')[0]);
        let sassFileContent = '';

        sassFileContent += `@import "compass/utilities/sprites/base";\n`;
        sassFileContent += `$icons: sprite-map("${platform}_${size}/*.png", $new-position: 0, $new-spacing: 0, $new-repeat: no-repeat);\n\n`;

        sassFileContent += `.emoji-${platform} {\n`
        sassFileContent += `  &.emoji,\n`;
        sassFileContent += `  .emoji {\n`;
        sassFileContent += `    display: inline-block;\n`;
        sassFileContent += `    overflow: hidden;\n`;
        sassFileContent += `    width: ${size}px;\n`;
        sassFileContent += `    height: ${size}px;\n`;
        sassFileContent += `  }\n`;
        sassFileContent += `  &.emoji-size-${size} {\n`;
        sassFileContent += `    &.emoji,\n`;
        sassFileContent += `    .emoji {\n`;
        sassFileContent += `      background: $icons no-repeat;\n`;
        sassFileContent += `      overflow: hidden;\n`;
        sassFileContent += `    width: ${size}px;\n`;
        sassFileContent += `    height: ${size}px;\n`;
        sassFileContent += `    }\n`;
        sassFileContent += `  }\n`;
        sassFileContent += `}\n`;

        cleaned.forEach((id) => {
          sassFileContent += `.emoji-${platform}.emoji-size-${size} {\n`
          sassFileContent += `  &.emoji-${id},\n`;
          sassFileContent += `  .emoji-${id} {\n`;
          sassFileContent += `    background-position: sprite-position($icons, '${id}');\n`;
          sassFileContent += `  }\n`;
          sassFileContent += `}\n`;
        });

        fs.writeFile(`${sass}/emoji-${platform}-${size}.scss`, sassFileContent, 'utf8');
      });
    });
  });
};
const buildImage = (imgs) => {
  let isCalled = false;

  if (!Array.isArray(imgs) || imgs.length === 0) {
    return ;
  }

  imgs.forEach(res => {
    platforms.forEach(platform => {
      if (typeof res[platform] !== 'undefined') {
        const tmpDir = `${images}/tmp/${platform}`;
        const dir = `${images}/${platform}`;
        const file = `${res.decimal}.png`;

        base64.decode(res[platform], `${tmpDir}/${file}`, (err) => {
          if (err) return;

          emojiSizes.forEach(size => {
            sharp(`${tmpDir}/${file}`)
              .resize(size)
              .toFile(`${dir}_${size}/${file}`)
              .then(() => {
                const { queue, process } = sharp.counters();
                console.log({ queue });
                if (queue === 0 && process === 0 && !isCalled) {
                  isCalled = true;
                  buildSassFile();
                }
              });
          });
        });
      }
    });
  });
};

const removeSkin = (code) => code.replace(RegExp(`(${skinTones.map(s => `,U\\+${s.toUpperCase()}`).join('|')})`, 'gi'), '');

const replaceUnicode = (code) => code.split('_').map(s => `U+${s.toUpperCase()}`).join(separator);

const detectSkinable = (code) => skinTones.filter(s => code.toLowerCase().includes(s) && code.includes('_')).length > 0;

const buildJsDeclaration = (result) => {
  const sortByCategories = {};
  let skinable = [];
  let noneSkinable = [];
  let remove = [];
  let ignore = { android: [], ios: [] };

  let jsFileContent = '';

  result.forEach(res => {
    const category = res.category;

    if (typeof res.ios !== 'undefined' || typeof res.android !== 'undefined') {
      if (typeof sortByCategories[category] === 'undefined') {
        sortByCategories[category] = [];
      }

      if (typeof res.ios !== 'undefined' && typeof res.android === 'undefined') {
        ignore.android.push(replaceUnicode(res.code));
      }

      if (typeof res.android !== 'undefined' && typeof res.ios === 'undefined') {
        ignore.ios.push(replaceUnicode(res.code));
      }

      if (res.code.includes('_fe0f_200d')) {
        remove.push(replaceUnicode(`${res.code.split('_fe0f_200d')[0]}`));
        remove.push(replaceUnicode(`${res.code.split('_200d')[0]}`));
        noneSkinable.push(removeSkin(replaceUnicode(res.code)));
      } else {
        if (res.code.includes('_fe0f_')) {
          remove.push(replaceUnicode(`${res.code.split('_fe0f_')[0]}`));
        }

        if (res.code.includes('200d')) {
          remove.push(replaceUnicode(res.code.split('_200d')[0]));
        }
      }

      if (detectSkinable(res.code)) {
        skinable.push(replaceUnicode(res.code));
        noneSkinable.push(removeSkin(replaceUnicode(res.code)));
      }

      sortByCategories[category].push(replaceUnicode(res.code));
    }
  });

  remove = remove.filter((v, i, a) => a.indexOf(v) === i);
  skinable = skinable.filter(s => !remove.includes(s)).filter((v, i, a) => a.indexOf(v) === i);
  noneSkinable = noneSkinable.filter(s => !remove.includes(s)).filter((v, i, a) => a.indexOf(v) === i);
  Object.keys(sortByCategories).forEach(cat => {
    sortByCategories[cat] = sortByCategories[cat].filter(s => !remove.includes(s)).filter((v, i, a) => a.indexOf(v) === i)
  });

  Object.keys(ignore).forEach(p => {
    ignore[p] = ignore[p].filter(s => !remove.includes(s)).filter((v, i, a) => a.indexOf(v) === i)
  });

  // fix detective emoji
  // noneSkinable.push('U+1F575,U+FE0F,U+200D,U+2642,U+FE0F');
  // noneSkinable.push('U+1F575,U+FE0F,U+200D,U+2640,U+FE0F');

  jsFileContent += `/**\n`;
  jsFileContent += ` * THIS FILE IS AUTOMATICALLY GENERATED - DON'T MODIFY HIM\n`;
  jsFileContent += ` */\n\n`;
  jsFileContent += `/* eslint-disable */\n`;
  jsFileContent += `export const skinTones = ${JSON.stringify(skinTones.map(s => (s !== 'none' ? `U+${s.toUpperCase()}` : s) .replace(/\_/g, separator)))};\n\n`;
  jsFileContent += `export const emojisList = ${JSON.stringify(sortByCategories)};\n\n`;
  jsFileContent += `export const emojisSkinable = ${JSON.stringify(skinable)};\n\n`;
  jsFileContent += `export const emojisSkinableBase = ${JSON.stringify(noneSkinable)};\n\n`;
  jsFileContent += `export const emojisIgnore = ${JSON.stringify(ignore)};\n`;

  fs.writeFileSync(`${images}/emojis.js`, jsFileContent, 'utf8');

  return result;
};

const crawlContent = (toCrawl) => {
  const result = [];
  const rows = toCrawl.match(/<tr[\s\S]*?<\/tr>/g);

  if (rows !== null) {
    const declarationKeys = Object.keys(declaration);
    let currentCategory = null;

    rows.forEach((row, rowIndex) => {
      const cols = row.match(/<td[\s\S]*?<\/td>/g);
      const ths = row.match(/<th[\s\S]*?<\/th>/g);

      if (cols !== null && cols.length === validRow) {
        rowResult = {};

        cols.forEach((col, colIndex) => {
          if (declarationKeys.includes((colIndex + 1).toString())) {
            const { key, rule, index, callback } = declaration[colIndex + 1];
            const get = rule.exec(col);
            const check = col.match(rule);

            if (get !== null && check.length > 0) {
              rowResult[key] = get[index];

              if (typeof callback === 'function') {
                rowResult = Object.assign(rowResult, callback(rowResult[key], key) || {});
              }
            }
          }
        });

        if (Object.keys(rowResult).length > 0) {
          rowResult.category = currentCategory;
          result.push(rowResult);
        }
      } else if (ths !== null) {
        if (row.includes('mediumhead')) {
          const name = /name="([^"]+)"/gmi.exec(row);
          const check = row.match(/name="([^"]+)"/gmi);

          if (name !== null && check.length > 0) {
            currentCategory = name[1].replace('&amp;_', '');
          }
        }
      }
    });
  }

  return result;
};

// buildSassFile();
buildJsDeclaration(crawlContent(content));
// cleanDirectory(() => buildImage(buildJsDeclaration(crawlContent(content))));